<?php
/**
 * Plugin Name: FP Test Plugin
 * Plugin URI:  https://www.flyingpylon.com
 * Description: A plugin for testing version control and deployment
 * Version:     1.0
 * Author:      Paul Woods
 * Author URI:  https://www.flyingpylon.com
 */

function fp_test() {
    // code goes here
}

?>